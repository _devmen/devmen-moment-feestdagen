# moment-feestdagen
With moment-feestdagen you are able to check whether or not a certain date is a Dutch holiday. The list is complete when it comes to official national Dutch holidays. Most regional holidays are not included (yet). moment-feestdagen is a [Moment.js](http://momentjs.com/) plugin.

## How to use moment-feestdagen?
1. Install moment-feestdagen to your package.json by running `npm install moment-feestdagen --save`
2. Make sure moment.js is also installed with `npm install moment --save`
3. Import both
```javascript
// ES6
import moment from 'moment'
import 'moment-feestdagen'

// node
const moment = require('moment');
const { isDutchHoliday } = require('moment-feestdagen');
````
4. Use [Moment.js](http://momentjs.com/) to convert any date format to a Moment object.
5. Use `isDutchHoliday()` on that Moment object, it takes two optional values
  - `string | Array<string>` for province codes
  - `boolean` for whether or not it should exclude holidays without an official day off

## Examples since version 1.0.0
```javascript
const isTodayDutchHoliday = moment().isDutchHoliday();
// returns an object with the name, boolean if its an official day off and an array with provincecodes where the holiday is in effect
// retruns false (boolean) if today is a holiday, but not an official day off
// retruns false (boolean) if today is not a holiday

const isDateDutchHoliday = moment('2019-05-05').isDutchHoliday();
// returns an object about 'Bevrijdingsdag' because it's a holiday in all provinces

const isDateDutchHolidayEverywhere = moment('2019-03-02').isDutchHoliday();
// returns false because it's not a holiday in all provinces

const isDateDutchHolidayInZuidHolland = moment('2019-03-02').isDutchHoliday('ZH');
// returns false because it's not a holiday in ZH

const isDateDutchHolidayInLimburg = moment('2019-03-02').isDutchHoliday('LI');
// returns an object because it's a holiday in LI

const isDateDutchHoliday = moment('2019-04-19').isDutchHoliday()
// returns false because Good Friday is not a official day off

const isDateDutchHoliday = moment('2019-04-19').isDutchHoliday([], false)
// returns an object because it's a holiday, and not an official day off
// remark: notice the empty array to enable the second parameter to be set with the boolean

// province codes:
// NH = Noord-Holland
// ZH = Zuid-Holland
// ZE = Zeeland
// NB = Noord-Brabant
// UT = Utrecht
// FL = Flevoland
// FR = Friesland
// GR = Groningen
// DR = Drenthe
// OV = Overijssel
// GE = Gelderland
// LI = Limburg
```

## Shoutout
[moment-feiertage](https://github.com/DaniSchenk/moment-feiertage) by [DaniSchenk](https://github.com/DaniSchenk)
This npm package heavily influenced and inspired this module. The way it works is very similar, but is aimed at German holidays.
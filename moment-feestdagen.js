(function (root, factory) {
	if (typeof exports === 'object') {
		module.exports = factory(require('moment'));
	} else if (typeof define === 'function' && define.amd) {
		define('moment-feestdagen', ['moment'], factory);
	} else {
		root.moment = factory(root.moment);
	}
}(this, function (moment) {
	if (typeof moment === 'undefined') {
		throw Error("Can't find moment");
	}

	moment.fn.isDutchHoliday = function (province, officialDaysOnly = true) {
		const year = this.year();
		const easter = moment(calculateEasterDate(year)).format();
		let holidays = retrieveHolidays(year, easter);
		
		if (officialDaysOnly) {
			holidays = holidays.filter(e => e.officialDayOff === true)
		}

		const holiday = holidays.filter(e => this.isSame(e.date, 'day'))

		return (Array.isArray(holiday) && holiday.length) ? holiday : false
	}

	const retrieveHolidays = (year, easter) => {
		return [
			{
				name: 'Nieuwjaarsdag',
				date: moment(year + '-01-01'),
				officialDayOff: true,
				province: []
			},
			{
				name: 'Goede Vrijdag',
				date: moment(easter).subtract(2, 'days'),
				officialDayOff: false,
				province: []
			},
			{
				name: 'Driekoningen',
				date: moment(year + '-01-06'),
				officialDayOff: false,
				province: []
			},
			{
				name: 'Eerste Carnavaldag',
				date: moment(easter).subtract(43, 'days'),
				officialDayOff: false,
				province: ['BR','LI']
			},
			{
				name: 'Tweede Carnavaldag',
				date: moment(easter).subtract(42, 'days'),
				officialDayOff: false,
				province: ['BR','LI']
			},
			{
				name: 'Derde Carnavaldag',
				date: moment(easter).subtract(41, 'days'),
				officialDayOff: false,
				province: ['BR','LI']
			},
			{
				name: 'Eerste Paasdag',
				date: moment(easter),
				officialDayOff: true,
				province: []
			},
			{
				name: 'Tweede Paasdag',
				date: moment(easter).add(1, 'days'),
				officialDayOff: true,
				province: []
			},
			{
				name: 'Koningsdag',
				date: (moment(year + '-04-27').day() !== 0) ? moment(year + '-04-27') : moment(year + '-04-26'),
				officialDayOff: true,
				province: []
			},
			{	
				name: 'Bevrijdingsdag',
				date: moment(year + '-05-05'),
				officialDayOff: true,
				province: []
			},
			{
				name: 'Hemelvaart',
				date: moment(easter).add(39, 'days'),
				officialDayOff: true,
				province: []
			},
			{
				name: 'Eerste Pinksterdag',
				date: moment(easter).add(49, 'days'),
				officialDayOff: true,
				province: []
			},
			{
				name: 'Tweede Pinksterdag',
				date: moment(easter).add(50, 'days'),
				officialDayOff: true,
				province: []
			},
			{
				name: 'Eerste Kerstdag',
				date: moment(year + '-12-25'),
				officialDayOff: true,
				province: []
			},
			{
				name: 'Tweede Kerstdag',
				date: moment(year + '-12-26'),
				officialDayOff: true,
				province: []
			}
		]
	}

	const calculateEasterDate = (Y) => {
		// Taken from: http://astro.nmsu.edu/~lhuber-leaphist.html
		// And: https://stackoverflow.com/questions/1284314/easter-date-in-javascript/1284335  
		
		const C = Math.floor(Y / 100);
		const N = Y - 19 * Math.floor(Y / 19);
		const K = Math.floor((C - 17) / 25);
		let I = C - Math.floor(C / 4) - Math.floor((C - K) / 3) + 19 * N + 15;
		I = I - 30 * Math.floor((I / 30));
		I = I - Math.floor(I / 28) * (1 - Math.floor(I / 28) * Math.floor(29 / (I + 1)) * Math.floor((21 - N) / 11));
		let J = Y + Math.floor(Y / 4) + I + 2 - C + Math.floor(C / 4);
		J = J - 7 * Math.floor(J / 7);
		const L = I - J;
		const M = 3 + Math.floor((L + 40) / 44);
		const D = L + 28 - 31 * Math.floor(M / 4);

		return Y + '-' + padout(M) + '-' + padout(D);
	}

	const padout = (number) => {
		return (number < 10) ? '0' + number : number;
	}

	return moment;
}));